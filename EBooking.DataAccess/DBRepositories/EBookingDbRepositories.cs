﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EBooking.DataAccess
{
    public class EBookingtDbRepositories : IEBookingDbRepositories
    {
        //private static string _conStr;
        //static EticketDbRepositories()
        //{
        //    IConfigurationBuilder builder = new ConfigurationBuilder();
        //    builder.AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), "eticketdb.json"));
        //    var root = builder.Build();
        //    _conStr = root.GetConnectionString("MpkgITRDbConnectionString");
        //}


        private readonly AppDbContext _dbContext;
        public EBookingtDbRepositories(AppDbContext context)
        {
            _dbContext = context;
            CategoryRep = new CategoryRepository(_dbContext);
            CoverTypeRep = new CoverTypeRepository(_dbContext); 
            ProductRep = new ProductRepository(_dbContext);
            CompanyRep=new CompanyRepository(_dbContext);  
            ShoppingCartRep = new ShoppingCartRepository(_dbContext);   
            ApplicationUserRep = new ApplicationUserRepository(_dbContext);
            OrderHeaderRep = new OrderHeaderRepostitory(_dbContext);
            OrderDetailRep = new OrderDetailRepository(_dbContext);
        }

        public  void Save()
        {
            _dbContext.SaveChanges();   
        }
        public ICategoryRepository CategoryRep { get; private set; }
        public ICoverTypeRepository CoverTypeRep { get; private set; }
        public IProductRepository ProductRep { get; private set; }
        public ICompanyRepository CompanyRep { get; private set; }

        public IShoppingCartRepository ShoppingCartRep { get; private set; }
        public IApplicationUserRepostiory ApplicationUserRep { get; private set; }  
        public IOrderDetailRepository OrderDetailRep { get; private set; }  
        public IOrderHeaderRepository OrderHeaderRep { get; private set; }
                
    }
}
