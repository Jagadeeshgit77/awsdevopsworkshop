﻿using Microsoft.AspNetCore.Mvc;
using EBooking.DataAccess;
using EBooking.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace EBooking.Controllers
{
    [Area("Admin")]
    public class ProductController : Controller
    {
        private readonly IEBookingDbRepositories _db;
        private readonly IWebHostEnvironment _hostEnvironment;
     
        public ProductController(IEBookingDbRepositories db, IWebHostEnvironment webHostEnvironment)
        {
            _db = db;
            _hostEnvironment = webHostEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }
        //Get
        public ActionResult Upsert(int? id)
        {
            ProductVM productVM = new()
            {
                Product = new(),
                CategoryList = _db.CategoryRep.GetAll().Select(
                                                               c => new SelectListItem
                                                               {
                                                                   Text = c.Name,
                                                                   Value = c.Id.ToString()
                                                               }),
                CoverTypeList = _db.CoverTypeRep.GetAll().Select(
                                                           c => new SelectListItem
                                                           {
                                                               Text = c.Name,
                                                               Value = c.Id.ToString()
                                                           })
            };
            if(id ==0 || id== null)
            {
                return View(productVM);
            }
            else
            {
                productVM.Product = _db.ProductRep.GetFirstOrDefault(u => u.Id == id);
                return View(productVM);
            }
           
        }
        [HttpPost]
        public IActionResult Upsert(ProductVM obj, IFormFile? file)
        {
            if (ModelState.IsValid)
            {
                string wwwRootPath = _hostEnvironment.WebRootPath;
                if (file != null)
                {
                    string fileName = Guid.NewGuid().ToString();
                    var uploads = Path.Combine(wwwRootPath, @"product-images");
                    var extension = Path.GetExtension(file.FileName);

                    if (obj.Product.ImageUrl != null)
                    {
                        var oldImagePath = Path.Combine(wwwRootPath, obj.Product.ImageUrl.TrimStart('\\'));
                        if (System.IO.File.Exists(oldImagePath))
                        {
                            System.IO.File.Delete(oldImagePath);
                        }
                    }

                    using (var fileStreams = new FileStream(Path.Combine(uploads, fileName + extension), FileMode.Create))
                    {
                        file.CopyTo(fileStreams);
                    }
                    obj.Product.ImageUrl = @"\product-images\" + fileName + extension;

                }
                if (obj.Product.Id == 0)
                {
                    _db.ProductRep.Add(obj.Product);
                }
                else
                {
                    _db.ProductRep.Update(obj.Product);
                }
                _db.Save();
                TempData["success"] = "Product created successfully";
                return RedirectToAction("Index");
            }
            return View(obj);
        }

        #region "API Calls"
        [HttpGet]
        public ActionResult GetAll()
        {
            var allProducts = _db.ProductRep.GetAll(includeProperties:"Category,CoverType");
            return Json(new {data=allProducts});    
        }

        [HttpDelete]
        public IActionResult Delete(int? id)
        {
            var product = _db.ProductRep.GetFirstOrDefault(u=> u.Id== id);
            if(product == null)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }

            var oldImagePath = Path.Combine(_hostEnvironment.WebRootPath, product.ImageUrl.TrimStart('\\'));
            if (System.IO.File.Exists(oldImagePath))
            {
                System.IO.File.Delete(oldImagePath);
            }

            _db.ProductRep.Remove(product);
            _db.Save();
            return Json(new { success = true, message = "Product deleted successfully" });
            
        }
        #endregion  
    }
}
