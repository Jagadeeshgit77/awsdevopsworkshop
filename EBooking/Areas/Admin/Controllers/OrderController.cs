﻿using Microsoft.AspNetCore.Mvc;
using EBooking.Models;
using EBooking.DataAccess;
using EBooking.Utilities;

namespace EBooking.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class OrderController : Controller
    {
        private readonly IEBookingDbRepositories _db;
        public OrderController(IEBookingDbRepositories db)
        {
            _db = db;
        }
        public IActionResult Index()
        {
            return View();  
        }

        #region APICalls
        [HttpGet]
        public IActionResult GetAll(string status)
        {
            var orderHeaders = _db.OrderHeaderRep.GetAll(includeProperties: "ApplicationUser");
            switch (status)
            {
                case "pending":
                    orderHeaders = orderHeaders.Where(u=> u.PaymentStatus == SD.PaymentStatusForDelayedPayment);
                    break;
                case "inprocess":
                    orderHeaders = orderHeaders.Where(u => u.OrderStatus == SD.StatusInprocess);
                    break;
                case "completed":
                    orderHeaders = orderHeaders.Where(u => u.OrderStatus == SD.StatusApproved);
                    break;
                case "approved":
                    orderHeaders = orderHeaders.Where(u => u.OrderStatus == SD.StatusApproved);
                    break;
                default:
                    break;

            }
            return Json(new { data = orderHeaders });
        }
        #endregion

    }



}
