﻿using Microsoft.AspNetCore.Mvc;
using EBooking.DataAccess;
using EBooking.Models;

namespace EBooking.Controllers
{
    [Area("Admin")]
    public class CategoryController : Controller
    {
        private readonly IEBookingDbRepositories _dbRep;
        private readonly ICategoryRepository _categoryRep;
        public CategoryController(IEBookingDbRepositories dbRep)
        {
            _dbRep = dbRep;
            _categoryRep = _dbRep.CategoryRep;
        }
     
        public IActionResult Index()
        {
            var categories = _categoryRep.GetAll();
            return View(categories);
        }
        //Get
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Category obj )
        {
            if(obj.Name == obj.DisplayOrder.ToString())
            {
                ModelState.AddModelError("CustomError", "Name and display order should not be same");
            }
            if(ModelState.IsValid)
            {
                _categoryRep.Add(obj);
                _dbRep.Save();
                TempData["success"] = "Category created successfully";
                return RedirectToAction("Index");
            }

            return View(obj);
        }
        //Get
        public ActionResult Edit(int? id)
        {
            var category = _categoryRep.GetFirstOrDefault(c => c.Id == id);  
            if(category == null)
            {
                return NotFound();
            }
            return View(category);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Category obj)
        {
            if (obj.Name == obj.DisplayOrder.ToString())
            {
                ModelState.AddModelError("CustomError", "Name and display order should not be same");
            }
            if (ModelState.IsValid)
            {
                _categoryRep.Update(obj);
                _dbRep.Save();
                TempData["success"] = "Category updated successfully";
                return RedirectToAction("Index");
            }

            return View(obj);
        }
        //Get
        public ActionResult Delete(int? id)
        {
            var category = _categoryRep.GetFirstOrDefault(c => c.Id == id);
            if (category == null)
            {
                return NotFound();
            }
            return View(category);
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePost(int? id)
        {
            var category = _categoryRep.GetFirstOrDefault(c => c.Id == id);
            if(category != null)
            {
                _categoryRep.Remove(category);
                _dbRep.Save();
                TempData["success"] = "Category deleted successfully";
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return NotFound();
            }
            return View(category);
        }

    }
}
